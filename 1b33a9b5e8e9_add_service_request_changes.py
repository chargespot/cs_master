"""Update for BetterOffice service request feature

Revision ID: 1b33a9b5e8e9
Revises: abe898a6c32a
Create Date: 2016-07-07 12:05:20.219775

"""

# revision identifiers, used by Alembic.
revision = '1b33a9b5e8e9'
down_revision = 'abe898a6c32a'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa

def upgrade():
    # Insert default value into web_user
    op.execute("""INSERT INTO web_user (id, first_name, username, email, user_group_id, client_id) VALUES (-1, 'Invalid', 'default', 'default', -1, 1);""")

    # Create and populate ticket_status
    ticket_status = op.create_table(
        'ticket_status',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('name', sa.VARCHAR(length=256), nullable=False)
        )
    op.bulk_insert(ticket_status,
            [
                { 'id': 1, 'name': 'New' },
                { 'id': 2, 'name': 'Assigned' },
                { 'id': 3, 'name': 'Resolved' }
            ]
        )
    # Add ticket_status column and constraint
    op.add_column('ticket', sa.Column('status_id', sa.Integer, nullable=False, server_default='1'))
    op.create_foreign_key('ticket_status_id_fkey', 'ticket', 'ticket_status', ['status_id'], ['id'])
    # Add constraint to assignee user ID
    op.add_column('ticket', sa.Column('assignee_user_id', sa.Integer, nullable=False, server_default='-1'))
    op.create_foreign_key('ticket_assignee_user_id_fkey', 'ticket', 'web_user', ['assignee_user_id'], ['id'])
    # Drop unnecessary nullable constraints
    op.alter_column('ticket', 'user_email', nullable=True)

    
    op.alter_column('ticket', 'open_date', new_column_name='open_timestamp')
    op.alter_column('ticket', 'close_date', new_column_name='close_timestamp')

    # Create and populate ticket_event_type
    ticket_event_type = op.create_table(
        'ticket_event_type',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('name', sa.VARCHAR(length=256), nullable=False)
    )
    op.bulk_insert(ticket_event_type,
            [
                { 'id': 1, 'name': 'Ticket Created' },
                { 'id': 2, 'name': 'Request Received' },
                { 'id': 3, 'name': 'Assignment' },
                { 'id': 4, 'name': 'Update Internal' },
                { 'id': 5, 'name': 'Update External' },
                { 'id': 6, 'name': 'Resolved' }
            ]
        )

    # Create ticket_event
    op.create_table(
        'ticket_event',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('type_id', sa.Integer, nullable=False, server_default='1'),
        sa.Column('sender_user_id', sa.Integer, nullable=False, server_default='-1'),
        sa.Column('recipients', sa.VARCHAR(128)),
        sa.Column('message', sa.String),
        sa.Column('ticket_id', sa.Integer, nullable=False),
        sa.Column('created', sa.DateTime, nullable=False)
        )
    op.create_foreign_key('ticket_event_type_id_fkey', 'ticket_event', 'ticket_event_type', ['type_id'], ['id'])
    op.create_foreign_key('ticket_event_sender_user_id_fkey', 'ticket_event', 'web_user', ['sender_user_id'], ['id'])
    op.create_foreign_key('ticket_event_ticket_id_fkey', 'ticket_event', 'ticket', ['ticket_id'], ['id'])

    # Create request
    op.create_table(
        'ticket_request',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('open_timestamp', sa.DateTime, nullable=False),
        sa.Column('submitter_user_id', sa.Integer, nullable=False, server_default='-1'),
        sa.Column('message', sa.String),
        sa.Column('image_url', sa.VARCHAR(256)),
        sa.Column('ticket_id', sa.Integer, nullable=False)
        )
    op.create_foreign_key('ticket_request_submitter_user_id_fkey', 'ticket_request', 'web_user', ['submitter_user_id'], ['id'])
    op.create_foreign_key('ticket_request_ticket_id_fkey', 'ticket_request', 'ticket', ['ticket_id'], ['id'])


def downgrade():
    op.alter_column('ticket', 'open_timestamp', new_column_name='open_date')
    op.alter_column('ticket', 'close_timestamp', new_column_name='close_date')
    #op.alter_column('ticket', 'user_email', nullable=False)

    op.drop_constraint('ticket_request_submitter_user_id_fkey', 'ticket_request', type_='foreignkey')
    op.drop_constraint('ticket_request_ticket_id_fkey', 'ticket_request', type_='foreignkey')
    op.drop_table('ticket_request')

    op.drop_constraint('ticket_status_id_fkey', 'ticket', type_='foreignkey') 
    op.drop_column('ticket', 'status_id')
    op.drop_table('ticket_status')

    op.drop_constraint('ticket_assignee_user_id_fkey', 'ticket', type_='foreignkey')
    op.drop_column('ticket', 'assignee_user_id')

    op.drop_constraint('ticket_event_type_id_fkey', 'ticket_event', type_='foreignkey')
    op.drop_table('ticket_event_type')

    op.drop_constraint('ticket_event_sender_user_id_fkey', 'ticket_event', type_='foreignkey')
    op.drop_constraint('ticket_event_ticket_id_fkey', 'ticket_event', type='foreignkey')
    op.drop_table('ticket_event')

    op.execute("""DELETE FROM web_user WHERE id = -1;""")
