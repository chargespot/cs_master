"""Create table tickets for concierge support

Revision ID: 95232d1d3a44
Revises: b7c95597b2d3
Create Date: 2016-06-22 17:52:42.101905

"""

# revision identifiers, used by Alembic.
revision = '95232d1d3a44'
down_revision = 'b7c95597b2d3'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():

    category_type = op.create_table(
        'ticket_category_type',
        sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=False),
        sa.Column('name', sa.String, nullable=False)
    )

    op.bulk_insert(category_type,
        [
            {'id': 1, 'name': 'Too Cold'},
            {'id': 2, 'name': 'Too Hot'},
            {'id': 3, 'name': 'Supplies'},
            {'id': 4, 'name': 'Dirty'},
            {'id': 5, 'name': 'Broken'},
            {'id': 6, 'name': 'Other'}
        ]
    )

    priority_type = op.create_table(
        'ticket_priority_type',
        sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=False),
        sa.Column('name', sa.String, nullable=False)
    )

    op.bulk_insert(priority_type,
        [
            {'id': 1, 'name': 'Low'},
            {'id': 2, 'name': 'Medium'},
            {'id': 3, 'name': 'High'}
        ]
    )

    op.create_table(
        'ticket',
        sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=True),
        sa.Column('message', sa.String),
        sa.Column('user_email', sa.String, nullable=False),
        sa.Column('open_date', sa.DateTime, nullable=False),
        sa.Column('close_date', sa.DateTime),
        sa.Column('location_name', sa.String, nullable=False),
        sa.Column('app_info', sa.String),
        sa.Column('image_url', sa.String),
        sa.Column('category_type_id', sa.Integer, sa.ForeignKey('ticket_category_type.id'), nullable=False),
        sa.Column('priority_type_id', sa.Integer, sa.ForeignKey('ticket_priority_type.id'))
    )


def downgrade():
    op.drop_table('ticket')
    op.drop_table('ticket_priority_type')
    op.drop_table('ticket_category_type')
