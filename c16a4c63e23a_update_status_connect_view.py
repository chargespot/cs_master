"""update status connect view

Revision ID: c16a4c63e23a
Revises: e557da6ec46c
Create Date: 2016-03-23 16:14:40.387708

"""

# revision identifiers, used by Alembic.
revision = 'c16a4c63e23a'
down_revision = 'e557da6ec46c'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.get_bind().execute("""
        DROP VIEW status_connect;
        CREATE OR REPLACE VIEW status_connect AS
        SELECT
            g0.client_id AS client_id,
            tx.id AS tx_device_id,
            c.name AS connect_name,
            c.key AS connect_key,
            tx.name AS tx_name,
            g0.name AS tx_0_group_name,
            hb.charge_status,
            COALESCE(s.activations, 0) AS activations,
            COALESCE(s.charge_seconds, 0) / 3600 AS usage_hr,
            COALESCE(s.charge_seconds, 0) %% 3600 / 60 AS usage_min,
            hb.last_heard,
            age(timezone('utc'::text, now()), hb.last_heard) AS last_heard_duration,
                CASE
                    WHEN (date_part('hour'::text, timezone('utc'::text, now()) - hb.last_heard) + date_part('day'::text, timezone('utc'::text, now()) - hb.last_heard) * 24::DOUBLE precision) < 24::DOUBLE PRECISION THEN 0
                    ELSE 1
                END AS warning,
                CASE
                    WHEN (date_part('hour'::text, timezone('utc'::text, now()) - hb.last_heard) + date_part('day'::text, timezone('utc'::text, now()) - hb.last_heard) * 24::DOUBLE precision) < 3::DOUBLE PRECISION THEN 0
                    ELSE 1
                END AS internal_warning,
            hb.created AS last_updated,
            hb.host,
            hb.port
           FROM tx_device tx
             JOIN tx_0_group g0 ON g0.id = tx.tx_0_group_id
             JOIN map_tx_device_connect m ON m.tx_device_id = tx.id
             JOIN connect_hb_cp hb ON hb.connect_id = m.connect_id
             JOIN connect_cp c ON c.id = hb.connect_id
             LEFT JOIN ui_stats_tx_device_cp s ON s.tx_device_id = tx.id
          ORDER BY tx.id;
    """)

def downgrade():
    op.get_bind().execute("""
        DROP VIEW status_connect;
        CREATE OR REPLACE VIEW status_connect AS
        SELECT
            tx.id AS tx_device_id,
            c.name AS connect_name,
            c.key AS connect_key,
            tx.name AS tx_name,
            g0.name AS tx_0_group_name,
            hb.charge_status,
            COALESCE(s.activations, 0) AS activations,
            COALESCE(s.charge_seconds, 0) / 3600 AS usage_hr,
            COALESCE(s.charge_seconds, 0) %% 3600 / 60 AS usage_min,
            hb.last_heard,
            age(timezone('utc'::text, now()), hb.last_heard) AS last_heard_duration,
                CASE
                    WHEN (date_part('hour'::text, timezone('utc'::text, now()) - hb.last_heard) + date_part('day'::text, timezone('utc'::text, now()) - hb.last_heard) * 24::DOUBLE precision) < 672::DOUBLE PRECISION THEN 0
                    ELSE 1
                END AS warning,
                CASE
                    WHEN (date_part('hour'::text, timezone('utc'::text, now()) - hb.last_heard) + date_part('day'::text, timezone('utc'::text, now()) - hb.last_heard) * 24::DOUBLE precision) < 3::DOUBLE PRECISION THEN 0
                    ELSE 1
                END AS internal_warning,
            hb.created AS last_updated,
            hb.host,
            hb.port
           FROM tx_device tx
             JOIN tx_0_group g0 ON g0.id = tx.tx_0_group_id
             JOIN map_tx_device_connect m ON m.tx_device_id = tx.id
             JOIN connect_hb_cp hb ON hb.connect_id = m.connect_id
             JOIN connect_cp c ON c.id = hb.connect_id
             LEFT JOIN ui_stats_tx_device_cp s ON s.tx_device_id = tx.id
          ORDER BY tx.id;
    """)
