"""Update hook_action_type pop-up to image

Revision ID: 9d670392d91c
Revises: 1923c9b0783e
Create Date: 2016-04-26 11:36:15.050176

"""

# revision identifiers, used by Alembic.
revision = '9d670392d91c'
down_revision = '1923c9b0783e'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.get_bind().execute("""
    UPDATE hook_action_type SET name = 'Image' WHERE id = 2;
    """)

def downgrade():
    op.get_bind().execute("""
    UPDATE hook_action_type SET name = 'Pop-Up' WHERE id = 2;
    """)
