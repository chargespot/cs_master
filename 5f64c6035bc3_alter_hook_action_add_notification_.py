"""Alter hook_action add notification title column

Revision ID: 5f64c6035bc3
Revises: 9d670392d91c
Create Date: 2016-04-26 13:41:22.418470

"""

# revision identifiers, used by Alembic.
revision = '5f64c6035bc3'
down_revision = '9d670392d91c'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('hook_action', sa.Column('notification_title', sa.String(20), nullable=False, server_default=""))

def downgrade():
    op.drop_column('hook_action', 'notification_title')
