"""Add type_office columns

Revision ID: b074b04aadb6
Revises: 5f64c6035bc3
Create Date: 2016-05-04 09:50:23.989234

"""

# revision identifiers, used by Alembic.
revision = 'b074b04aadb6'
down_revision = '5f64c6035bc3'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.get_bind().execute("""
        ALTER TABLE web_user ADD COLUMN type_office BOOLEAN NOT NULL DEFAULT false;
        ALTER TABLE tx_1_group ADD COLUMN type_office BOOLEAN NOT NULL DEFAULT false;
        ALTER TABLE tx_1_group ADD COLUMN image_url VARCHAR(256);
        """)

def downgrade():
    op.get_bind().execute("""
        ALTER TABLE web_user DROP COLUMN type_office;
        ALTER TABLE tx_1_group DROP COLUMN type_office;
        ALTER TABLE tx_1_group DROP COLUMN image_url;
        """)
