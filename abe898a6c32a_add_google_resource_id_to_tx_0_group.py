"""Add google resource id to tx_0_group

Revision ID: abe898a6c32a
Revises: bf1100f4aaa7
Create Date: 2016-06-29 16:02:34.806123

"""

# revision identifiers, used by Alembic.
revision = 'abe898a6c32a'
down_revision = 'bf1100f4aaa7'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('tx_0_group',
        sa.Column('google_resource_id', sa.String)
    )


def downgrade():
    op.drop_column('tx_0_group', 'google_resource_id')
