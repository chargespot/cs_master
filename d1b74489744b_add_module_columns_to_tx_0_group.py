"""Add module columns to tx_0_group

Revision ID: d1b74489744b
Revises: 8b01f92881a5
Create Date: 2016-06-02 17:46:58.506120

"""

# revision identifiers, used by Alembic.
revision = 'd1b74489744b'
down_revision = '8b01f92881a5'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('tx_0_group',
        sa.Column('engage', sa.Boolean, server_default='0')
    )

    op.add_column('tx_0_group',
        sa.Column('calendar', sa.Boolean, server_default='0')
    )

def downgrade():
    op.drop_column('tx_0_group', 'engage')
    op.drop_column('tx_0_group', 'calendar')
