"""create table login_log

Revision ID: 24baeb4ff6fc
Revises: 962da779d87a
Create Date: 2016-06-06 14:37:49.407801

"""

# revision identifiers, used by Alembic.
revision = '24baeb4ff6fc'
down_revision = '962da779d87a'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy import TIMESTAMP, func, ForeignKey



def upgrade():
    # Create primary key for login_log first.
    op.create_table(
        'login_log',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('login_timestamp', TIMESTAMP, server_default=func.now(timezone="UTC"), nullable=False),
        sa.Column('web_user_id',  sa.Integer, ForeignKey('web_user.id'), nullable=False)
    )

    # create foreign key web_user_is references web_user.id
    #op.create_foreign_key("fk_web_user_id", "login_log" "web_user", ["web_user_id"], ["id"])


def downgrade():
    op.drop_constraint('login_log_web_user_id_fkey', 'login_log')
    op.drop_table('login_log')
