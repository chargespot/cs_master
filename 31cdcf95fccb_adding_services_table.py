"""Adding services table

Revision ID: 31cdcf95fccb
Revises: 
Create Date: 2016-06-08 14:15:26.116377

"""

# revision identifiers, used by Alembic.
revision = '31cdcf95fccb'
down_revision = '24baeb4ff6fc'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
	op.create_table(
		'service',
		sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
		sa.Column('name', sa.VARCHAR(length=256), nullable=False),
		sa.Column('api_name', sa.VARCHAR(length=256)),
        sa.Column('api_url', sa.VARCHAR(length=256)),
        sa.Column('api_id', sa.VARCHAR(length=256)),
        sa.Column('icon_url', sa.VARCHAR(length=256)),
		sa.Column('icon_name', sa.VARCHAR(length=256)),
		sa.Column('action', sa.VARCHAR(length=256))
	)			


	op.create_table(
		'tx_0_group_service_mapping',
		sa.Column('tx_0_group_id', sa.Integer, nullable=False),
		sa.Column('service_id', sa.Integer, nullable=False),
		sa.Column('api_resource_key', sa.VARCHAR(length=256))
	)
	
	op.get_bind().execute(
	"""
		INSERT INTO service (name, icon_name, action)
		VALUES ('Book Room', 'calendar', 'calendar');

	""")

	op.get_bind().execute(
	"""
        INSERT INTO tx_0_group_service_mapping(tx_0_group_id, service_id, api_resource_key)
        VALUES (22, 1, '31633162137');
    """)
def downgrade():
	op.drop_table('service')
	op.drop_table('tx_0_group_service_mapping')
