"""Add tx_0_group to web_user mapping

Revision ID: 962da779d87a
Revises: d1b74489744b
Create Date: 2016-06-02 17:53:01.684006

"""

# revision identifiers, used by Alembic.
revision = '962da779d87a'
down_revision = 'd1b74489744b'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa

def upgrade():

    op.create_table(
        'tx_0_groups_web_users_mapping',
        sa.Column('tx_0_group_id', sa.Integer, nullable=False),
        sa.Column('user_id', sa.Integer, nullable=False)
    )

    op.create_foreign_key("fk_group_mapping_to_web_user_users_mapping_id", "tx_0_groups_web_users_mapping", "web_user", ["user_id"], ["id"])
    op.create_foreign_key("fk_web_user_users_to_group_mapping_id", "tx_0_groups_web_users_mapping", "tx_0_group", ["tx_0_group_id"], ["id"])

def downgrade():
    op.drop_table('tx_0_groups_web_users_mapping')