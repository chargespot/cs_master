"""create user settings table

Revision ID: e557da6ec46c
Revises: d37a146961f6
Create Date: 2016-03-18 10:58:08.916409

"""

# revision identifiers, used by Alembic.
revision = 'e557da6ec46c'
down_revision = 'd37a146961f6'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():

    # Create primary key for web_user first.
    op.create_primary_key("pk_web_user", "web_user", ["id"])

    op.create_table(
        'web_user_settings',
        sa.Column('user_id', sa.Integer, nullable=False),
        sa.Column('alert_frequency_id', sa.Integer, nullable=False)
    )

    op.create_foreign_key("fk_web_user_settings_user_id", "web_user_settings", "web_user", ["user_id"], ["id"])
    op.create_foreign_key("fk_web_user_settings_alert_frequency_id", "web_user_settings", "alert_frequency", ["alert_frequency_id"], ["id"])

    #Add a default web_user_setting with frequency type for all web_users.
    op.get_bind().execute("""
        INSERT INTO web_user_settings (
            user_id,
            alert_frequency_id
        )
        SELECT
            id,
            0
        FROM web_user;
    """)

def downgrade():
    op.drop_table('web_user_settings')
    op.drop_constraint("pk_web_user", 'web_user')

