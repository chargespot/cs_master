"""Create table role and add to web_user

Revision ID: b6367c00d961
Revises: ca4a006e4818
Create Date: 2016-05-17 14:47:09.244344

"""

# revision identifiers, used by Alembic.
revision = 'b6367c00d961'
down_revision = '78ba3e9c32b6'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():

    #Add new role table and role data.
    role_table = op.create_table(
        'role',
        sa.Column('id', sa.Integer, nullable=False, primary_key=True, autoincrement=False),
        sa.Column('name', sa.VARCHAR(length=100), nullable=False)
    )

    op.bulk_insert(role_table,
        [
            {'id': 1, 'name':'ChargeSpot Admin'},
            {'id': 2, 'name':'Admin'},
            {'id': 3, 'name':'User'},
        ]
    )

    #Add role to web_user table.
    op.add_column('web_user',
        sa.Column('role_id', sa.Integer, sa.ForeignKey('role.id'), nullable=False, server_default='3')
    )

def downgrade():
    op.drop_column('web_user', 'role_id')
    op.drop_table('role')

