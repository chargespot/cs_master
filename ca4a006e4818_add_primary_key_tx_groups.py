"""Add primary key tx_groups

Revision ID: ca4a006e4818
Revises: b074b04aadb6
Create Date: 2016-05-04 13:30:26.729543

"""

# revision identifiers, used by Alembic.
revision = 'ca4a006e4818'
down_revision = 'b074b04aadb6'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_primary_key("pk_tx_0_group", "tx_0_group", ["id"])
    op.create_primary_key("pk_tx_1_group", "tx_1_group", ["id"])

def downgrade():
    op.drop_constraint("pk_tx_0_group", 'tx_0_group')
    op.drop_constraint("pk_tx_1_group", 'tx_1_group')
