"""create alert log table

Revision ID: d37a146961f6
Revises: 
Create Date: 2016-03-17 13:36:50.018726

"""

# revision identifiers, used by Alembic.
revision = 'd37a146961f6'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa

def upgrade():

    # Create primary key for tx_device first.
    op.create_primary_key("pk_tx_device", "tx_device", ["id"])

    op.create_table(
        'alert_log',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('tx_device_id', sa.Integer, nullable=False),
        sa.Column('last_heard', sa.TIMESTAMP(timezone=False), nullable=False),
        sa.Column('open_date', sa.TIMESTAMP(timezone=False), nullable=False),
        sa.Column('status', sa.Integer),
        sa.Column('last_charge', sa.TIMESTAMP(timezone=False)),
        sa.Column('resolve_category', sa.Integer),
        sa.Column('sys_ack_date', sa.TIMESTAMP(timezone=False)),
        sa.Column('report_date', sa.TIMESTAMP(timezone=False)),
        sa.Column('resolve_date', sa.TIMESTAMP(timezone=False)),
        sa.Column('report_date', sa.TIMESTAMP(timezone=False)),
        sa.Column('resolve_date', sa.TIMESTAMP(timezone=False)),
        sa.Column('created', sa.TIMESTAMP(timezone=False)),
        sa.Column('last_updated', sa.TIMESTAMP(timezone=False))
    )

    alert_frequency_table = op.create_table(
        'alert_frequency',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.VARCHAR(length=100), nullable=False)
    )

    op.bulk_insert(alert_frequency_table,
        [
            {'id': 0, 'name':'Never'},
            {'id': 1, 'name':'Immediate'},
            {'id': 2, 'name':'Daily'},
            {'id': 3, 'name':'Weekly'},
        ]
    )

    op.create_foreign_key("fk_alert_tx_device_id", "alert_log", "tx_device", ["tx_device_id"], ["id"])

def downgrade():
    op.drop_table('alert_log')
    op.drop_table('alert_frequency')
    op.drop_constraint("pk_tx_device", 'tx_device')
