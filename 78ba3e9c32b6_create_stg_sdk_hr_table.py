"""create stg_sdk_hr table

Revision ID: 78ba3e9c32b6
Revises: ca4a006e4818
Create Date: 2016-05-25 14:38:49.541476

"""

# revision identifiers, used by Alembic.
revision = '78ba3e9c32b6'
down_revision = 'ca4a006e4818'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.get_bind().execute("""
        CREATE TABLE stg_sdk_hr_cp (
            id BIGINT,
            log_timestamp TIMESTAMP WITHOUT TIME ZONE, 
            tx_0_group_id INTEGER,
            tx_device_id INTEGER,
            app_user_id INTEGER,
            app_token_id INTEGER,
            new_user BOOLEAN,
            event_start_count INTEGER,
            event_stop_count INTEGER,
            created TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW () AT TIME ZONE 'utc')
        );
    """)


def downgrade():
    op.get_bind().execute("""
        DROP TABLE stg_sdk_hr_cp;        
    """)
