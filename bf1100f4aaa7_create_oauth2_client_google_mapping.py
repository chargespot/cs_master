"""Create oauth2 client google mapping

Revision ID: bf1100f4aaa7
Revises: 95232d1d3a44
Create Date: 2016-06-24 15:45:52.665111

"""

# revision identifiers, used by Alembic.
revision = 'bf1100f4aaa7'
down_revision = '95232d1d3a44'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('oauth2_client_token',
        sa.Column('client_id', sa.Integer, sa.ForeignKey('role.id'), primary_key=True, nullable=False),
        sa.Column('credentials_json', sa.String, nullable=False))


def downgrade():
    op.drop_table('oauth2_client_token')

