"""create_table_stg_charge_hr_cp

Revision ID: 1923c9b0783e
Revises: 32b7e29c42bf
Create Date: 2016-04-12 18:25:10.593649

"""

# revision identifiers, used by Alembic.
revision = '1923c9b0783e'
down_revision = '32b7e29c42bf'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.get_bind().execute("""
        CREATE TABLE stg_charge_hr_cp (
            id integer NOT NULL,
            connect_id integer,
            log_timestamp timestamp without time zone,
            charge_seconds integer,
            activations_raw integer,
            created timestamp without time zone DEFAULT timezone('utc'::text, now()),
            activations_users integer
        );

        ALTER TABLE stg_charge_hr_cp OWNER TO cs_db;

        CREATE SEQUENCE stg_charge_hr_cp_id_seq
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;

        ALTER TABLE stg_charge_hr_cp_id_seq OWNER TO cs_db;

        ALTER SEQUENCE stg_charge_hr_cp_id_seq OWNED BY stg_charge_hr_cp.id;

        ALTER TABLE ONLY stg_charge_hr_cp ALTER COLUMN id SET DEFAULT nextval('stg_charge_hr_cp_id_seq'::regclass);

        """)

def downgrade():
    op.get_bind().execute("""
        DROP TABLE stg_charge_hr_cp;
    """)
