"""Update tx_group with room requirements

Revision ID: b7c95597b2d3
Revises: 31cdcf95fccb
Create Date: 2016-06-21 12:33:25.843955

"""

# revision identifiers, used by Alembic.
revision = 'b7c95597b2d3'
down_revision = '31cdcf95fccb'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('tx_0_group', sa.Column('projector', sa.Boolean, server_default='0', nullable=False))
    op.add_column('tx_0_group', sa.Column('television', sa.Boolean, server_default='0', nullable=False))
    op.add_column('tx_0_group', sa.Column('phone', sa.Boolean, server_default='0', nullable=False))
    op.add_column('tx_0_group', sa.Column('camera', sa.Boolean, server_default='0', nullable=False))
    op.add_column('tx_0_group', sa.Column('whiteboard', sa.Boolean, server_default='0', nullable=False))
    op.add_column('tx_0_group', sa.Column('capacity', sa.Integer, nullable=True))
    op.add_column('tx_0_group', sa.Column('google_resource_email', sa.String(254)))

    op.add_column('tx_1_group', sa.Column('floor', sa.Integer))


def downgrade():
    op.drop_column('tx_0_group', 'projector')
    op.drop_column('tx_0_group', 'television')
    op.drop_column('tx_0_group', 'phone')
    op.drop_column('tx_0_group', 'camera')
    op.drop_column('tx_0_group', 'whiteboard')
    op.drop_column('tx_0_group', 'capacity')
    op.drop_column('tx_0_group', 'google_resource_email')

    op.drop_column('tx_1_group', 'floor')
