"""Add tx_disable to connect_hb table

Revision ID: 3f4207fc26f4
Revises: 1b33a9b5e8e9
Create Date: 2016-07-22 14:15:57.734557

"""

# revision identifiers, used by Alembic.
revision = '3f4207fc26f4'
down_revision = '1b33a9b5e8e9'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("ALTER TABLE connect_hb_cp ADD COLUMN tx_disable SMALLINT;")
    op.execute("""
        CREATE OR REPLACE VIEW status_connect
        AS
        SELECT g0.client_id,
            tx.id AS tx_device_id,
            c.name AS connect_name,
            c.key AS connect_key,
            tx.name AS tx_name,
            g0.name AS tx_0_group_name,
            hb.charge_status,
            COALESCE(s.activations, 0) AS activations,
            COALESCE(s.charge_seconds, 0) / 3600 AS usage_hr,
            COALESCE(s.charge_seconds, 0) % 3600 / 60 AS usage_min,
            hb.last_heard,
            age(timezone('utc'::text, now()), hb.last_heard) AS last_heard_duration,
                CASE
                    WHEN (date_part('hour'::text, timezone('utc'::text, now()) - hb.last_heard) + date_part('day'::text, timezone('utc'::text, now()) - hb.last_heard) * 24::double precision) < 48::double precision THEN 0
                    ELSE 1
                END AS warning,
                CASE
                    WHEN (date_part('hour'::text, timezone('utc'::text, now()) - hb.last_heard) + date_part('day'::text, timezone('utc'::text, now()) - hb.last_heard) * 24::double precision) < 6::double precision THEN 0
                    ELSE 1
                END AS internal_warning,
            hb.created AS last_updated,
            hb.host,
            hb.port,
            hb.tx_disable
           FROM tx_device tx
             JOIN tx_0_group g0 ON g0.id = tx.tx_0_group_id
             JOIN map_tx_device_connect m ON m.tx_device_id = tx.id
             JOIN connect_hb_cp hb ON hb.connect_id = m.connect_id
             JOIN connect_cp c ON c.id = hb.connect_id
             LEFT JOIN ui_stats_tx_device_cp s ON s.tx_device_id = tx.id
            ORDER BY tx.id;
        """)


def downgrade():
    op.execute("DROP VIEW status_connect;")
    op.execute("""
        CREATE OR REPLACE VIEW status_connect
        AS
        SELECT g0.client_id,
            tx.id AS tx_device_id,
            c.name AS connect_name,
            c.key AS connect_key,
            tx.name AS tx_name,
            g0.name AS tx_0_group_name,
            hb.charge_status,
            COALESCE(s.activations, 0) AS activations,
            COALESCE(s.charge_seconds, 0) / 3600 AS usage_hr,
            COALESCE(s.charge_seconds, 0) % 3600 / 60 AS usage_min,
            hb.last_heard,
            age(timezone('utc'::text, now()), hb.last_heard) AS last_heard_duration,
                CASE
                    WHEN (date_part('hour'::text, timezone('utc'::text, now()) - hb.last_heard) + date_part('day'::text, timezone('utc'::text, now()) - hb.last_heard) * 24::double precision) < 48::double precision THEN 0
                    ELSE 1
                END AS warning,
                CASE
                    WHEN (date_part('hour'::text, timezone('utc'::text, now()) - hb.last_heard) + date_part('day'::text, timezone('utc'::text, now()) - hb.last_heard) * 24::double precision) < 6::double precision THEN 0
                    ELSE 1
                END AS internal_warning,
            hb.created AS last_updated,
            hb.host,
            hb.port
           FROM tx_device tx
             JOIN tx_0_group g0 ON g0.id = tx.tx_0_group_id
             JOIN map_tx_device_connect m ON m.tx_device_id = tx.id
             JOIN connect_hb_cp hb ON hb.connect_id = m.connect_id
             JOIN connect_cp c ON c.id = hb.connect_id
             LEFT JOIN ui_stats_tx_device_cp s ON s.tx_device_id = tx.id
            ORDER BY tx.id;
        """)
    op.execute("ALTER TABLE connect_hb_cp DROP COLUMN tx_disable;")
