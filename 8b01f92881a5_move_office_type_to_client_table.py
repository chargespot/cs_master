"""Move office type to client table

Revision ID: 8b01f92881a5
Revises: b6367c00d961
Create Date: 2016-06-02 17:19:47.233271

"""

# revision identifiers, used by Alembic.
revision = '8b01f92881a5'
down_revision = 'b6367c00d961'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa

def upgrade():

    CLIENT_TYPE_VENUE = '1'
    CLIENT_TYPE_OFFICE = '2'

    client_type = op.create_table(
        'client_type',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('name', sa.String, nullable=False)
    )

    op.bulk_insert(client_type,
        [
            {'id': CLIENT_TYPE_VENUE, 'name': 'Venue'},
            {'id': CLIENT_TYPE_OFFICE, 'name': 'Office'}
        ]
    )

    op.add_column('client',
        sa.Column('type_id', sa.Integer, sa.ForeignKey('client_type.id'), server_default=CLIENT_TYPE_VENUE)
    )

    op.drop_column('web_user', 'type_office')
    op.drop_column('tx_1_group', 'type_office')


def downgrade():
    op.drop_column('client', 'type_id')
    op.drop_table('client_type')

    op.get_bind().execute("""
        ALTER TABLE web_user ADD COLUMN type_office BOOLEAN NOT NULL DEFAULT false;
        ALTER TABLE tx_1_group ADD COLUMN type_office BOOLEAN NOT NULL DEFAULT false;
    """)
