"""update hooks remove nulls and charging conflicts

Revision ID: 32b7e29c42bf
Revises: c16a4c63e23a
Create Date: 2016-04-01 15:20:05.539717

"""

# revision identifiers, used by Alembic.
revision = '32b7e29c42bf'
down_revision = 'c16a4c63e23a'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_primary_key("pk_hook", "hook", ["id"])

    op.get_bind().execute("""
    UPDATE hook SET delay_seconds = 0 WHERE delay_seconds IS NULL;

    UPDATE hook SET enabled = FALSE WHERE id IN (
    SELECT id FROM hook WHERE hook_type_id = 5 AND enabled = TRUE AND hook.tx_0_group_id IN (
    SELECT tx_0_group_id FROM hook WHERE hook_type_id = 4 AND enabled = TRUE AND delay_seconds IS NOT NULL));
    """)

    op.alter_column("hook", "delay_seconds", nullable=False, server_default='0')

def downgrade():
    op.alter_column("hook", "delay_seconds", nullable=True)

    op.get_bind().execute("""
    UPDATE hook SET delay_seconds = NULL WHERE delay_seconds = 0;
    """)

    op.drop_constraint(
		name ="pk_hook",
		table_name="hook",
		type_="primary"
	)
